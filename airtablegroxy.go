package airtablegroxy

//Config ...
type Config struct {
	Development bool
	Port        string
	Airtable    *AirtableConfig
}
