package http

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/fabiofcferreira/airtablegroxy"
)

//Serve creates all the routing and starts the HTTP server
func Serve(c *airtablegroxy.Config) {
	r := mux.NewRouter()

	// r.HandleFunc("/get",)

	log.Fatal(http.ListenAndServe(":"+c.Port, r))
}
