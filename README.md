# AirTable Go Proxy
AirTable Go Proxy is an API proxy for AirTable powered by Go. Its main purpose is to help developers improve
their app's security. Instead of using the API key in the frontend completely in favor of hacks and abuse, use
AirTable Go Proxy.
