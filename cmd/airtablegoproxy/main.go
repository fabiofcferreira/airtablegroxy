package main

import (
	"fmt"
	"runtime"
	"strconv"

	"gitlab.com/fabiofcferreira/airtablegroxy"
	h "gitlab.com/fabiofcferreira/airtablegroxy/http"
)

func main() {
	// Execute with all of the CPUs available
	runtime.GOMAXPROCS(runtime.NumCPU())

	cfg, err := loadConfig("config.json")
	if err != nil {
		panic(err)
	} else {
		fmt.Println("Config loaded")
	}

	c := &airtablegroxy.Config{
		Development: cfg.Development,
		Port:        strconv.Itoa(cfg.Port),
		Airtable: &airtablegroxy.AirtableConfig{
			APIKey: cfg.Airtable.APIKey,
		},
	}

	h.Serve(c)
}
