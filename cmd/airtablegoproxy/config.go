package main

import (
	"encoding/json"
	"os"
)

type config struct {
	Development bool
	Scheme      string
	Domain      string
	Port        int
	Airtable    struct {
		APIKey string
	}
}

func loadConfig(path string) (*config, error) {
	file := &config{}

	configFile, err := os.Open(path)
	if err != nil {
		return file, err
	}

	jsonParser := json.NewDecoder(configFile)
	err = jsonParser.Decode(&file)

	return file, err
}
